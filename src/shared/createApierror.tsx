import { notification } from 'antd'
import { AxiosError } from 'axios'
import React from 'react'
import { IApiError } from '../interfaces/IApiError'

export const createApiError = ({
  request,
  response,
  message: AxiosMessage
}: AxiosError) => {
  if (!response)
    return notification.error({
      message: 'Não foi possível carregar',
      description: AxiosMessage
    })

  const { code, message, errors } = response.data as IApiError

  if (code >= 500)
    return notification.error({
      message: 'Erro interno',
      description: message
    })

  notification.error({
    message: AxiosMessage,
    description: (
      <>
        <div>{message}</div>
        {Array.isArray(errors) &&
          errors?.map((error, i) => <div key={i}>{error.message}</div>)}
      </>
    )
  })
}
