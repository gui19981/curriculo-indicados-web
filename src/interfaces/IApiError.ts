export interface IApiError {
  name: string
  message: string
  code: number
  className: string
  errors: IErrorDescription[] | {}
}

interface IErrorDescription {
  message: string
  type: string
  path: string
  value: string
  origin: string
  instance: Object
  validatorKey: string
  validatorName: string
  validatorArgs: []
}
