import React, { FunctionComponent } from 'react'
import { PageHeader } from '../../components/styled-components'
import { useAsPage } from '../../hooks/useAsPage'

const Home: FunctionComponent = () => {
  useAsPage('home')

  return (
    <PageHeader>
      <img
        alt='Pessoas felizes'
        src={require('../../assets/images/home-img.jpg')}
        style={{ maxHeight: 400 }}
      ></img>
    </PageHeader>
  )
}

export default Home
