import { Button, Col } from 'antd'
import React, { FunctionComponent } from 'react'
import {
  Link,
  Route,
  Switch,
  useHistory,
  useRouteMatch
} from 'react-router-dom'
import { Empty } from '../../components/Empty'
import {
  PageHeader,
  PageHeaderTitle,
  Panel,
  PanelTitle
} from '../../components/styled-components'
import { useAsPage } from '../../hooks/useAsPage'
import { UserCreate } from './UserCreate'
import { UserEdit } from './UserEdit'
import { UserList } from './UserList'

const UserIndex: FunctionComponent = () => {
  useAsPage('usuarios', 'Genrenciar Usuários')
  const { push } = useHistory()
  const { path } = useRouteMatch()

  return (
    <>
      <PageHeader className='page-title'>
        <PageHeaderTitle>Gerenciamento de Usuários</PageHeaderTitle>
      </PageHeader>
      <Col span={12}>
        <Panel>
          <PanelTitle>
            Usuários
            <Link to={`${path}/novo`}>
              <Button type='primary' onClick={() => push(`${path}/novo)`)}>
                Adicionar
              </Button>
            </Link>
          </PanelTitle>
          <UserList />
        </Panel>
      </Col>
      <Col span={12}>
        <Switch>
          <Route exact path={path} component={() => <Empty name='usuario' />} />
          <Route path={`${path}/editando/:id`} component={UserEdit} />
          <Route path={`${path}/novo`} component={UserCreate} />
        </Switch>
      </Col>
    </>
  )
}

export default UserIndex
