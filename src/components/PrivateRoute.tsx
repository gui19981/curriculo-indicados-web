import React, { FunctionComponent, useContext } from 'react'
import { AuthStoreContext } from '../stores/AuthStore'
import { Route, Redirect, RouteProps } from 'react-router-dom'
import { observer } from 'mobx-react'

interface IPrivateRoute extends RouteProps {
  to: Array<'admin' | 'mod' | 'user'>
}

export const PrivateRoute: FunctionComponent<IPrivateRoute> = observer(
  ({ children, to, ...rest }) => {
    const { isAuth, decodedToken } = useContext(AuthStoreContext)

    const matchRole = to.some(role => role === decodedToken?.role)

    if (!isAuth || !matchRole) return <Redirect to='/' />

    return <Route {...rest} />
  }
)
